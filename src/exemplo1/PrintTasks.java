/**
 * Exemplo1: Programacao com threads
 * Autor: Lucio Agostinho Rocha
 * Willian Alberto Lauber
 * Ultima modificacao: 07/08/2017
 */
package exemplo1;

import java.util.Random;
//import java.util.UUID;

/**
 *@author  Willian Alberto Lauber
 * @author Lucio
 */
public class PrintTasks implements Runnable {

    private final int sleepTime; //tempo de adormecimento aleatorio para a thread
    private final String taskName; //nome da tarefa
    private final static Random generator = new Random();
//    private final String id;
    private int id;
    public PrintTasks(String name, int id){
        taskName = name;
//        id = UUID.randomUUID().toString();
        //Tempo aleatorio entre 0 e 5 segundos
        sleepTime = generator.nextInt(1000); //milissegundos
    }
    public int getId(){
        return this.id;
    }
    @Override
    public void run(){
        try{
            System.out.printf("Tarefa: %s dorme por %d ms\n", taskName, sleepTime);
            //Estado de ESPERA SINCRONIZADA
            //Nesse ponto, a thread perde o processador, e permite que
            //outra thread execute
            if(getId()%2 == 1)
                Thread.sleep(sleepTime);
        } catch (InterruptedException ex){
            System.out.printf("%s %s\n", taskName, "terminou de maneira inesperada.");
        }
       System.out.printf("%s acordou!\n", taskName);
    }
       
}
